module gitee.com/qlanwl/goadmincore/plugins/logger/logrus

go 1.17

require (
	github.com/sirupsen/logrus v1.8.0
)

replace gitee.com/qlanwl/goadmincore => ../../../
