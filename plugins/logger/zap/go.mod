module gitee.com/qlanwl/goadmincore/plugins/logger/zap

go 1.17

replace gitee.com/qlanwl/goadmincore => ../../../

require (
	gitee.com/qlanwl/goadmincore v0.0.0-20211005162057-39bc93da1938
	go.uber.org/zap v1.19.1
)

require (
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
)
